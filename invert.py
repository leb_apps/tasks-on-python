def invert(a_dict):
	return dict([(value, key) for key, value in a_dict.items()])

print invert({'a': '1', 'b': '2', 'c': '3'})