def sum_args(*args):
	res = 0
	for arg in args:
		if hasattr(arg, '__len__'):
			res += sum_args(*arg)
		else:
			res += arg
	return res

print sum_args(1, 2, 3)
print sum_args([1, 2, 3])
print sum_args(([1, 2], 3), [[4]], (5, 6))
